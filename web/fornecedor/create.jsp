<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="campo" tagdir="/WEB-INF/tags" %>

<body>
<jsp:include page="../header.jsp" />
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h2>Criando um Fornecedor</h2>
    </div>
</div>
<div>
    <form class="col-md-8 col-md-offset-2" method="post" action="/fornecedor/create">

        <div class="row">
            <campo:texto id="representante" css="col-md-12" label="Representante" name="representante"
                         placeholder="Representante" tipo="text" />
        </div>

        <div class="row" >
            <campo:texto id="razaoSocial" css="col-md-6" label="Razão Social:" placeholder="Razão Social" name="razaoSocial" />
            <campo:texto id="cnpj" css="col-md-6" label="CNPJ:" placeholder="CNPJ" name="cnpj" />
        </div>
        <div class="row">
            <campo:texto id="nomeFantasia" css="col-md-6" label="Nome Fantasia:" placeholder="Nome Fantasia" name="nomeFantasia" />
            <campo:texto id="telefone" css="col-md-6" label="Telefone:" placeholder="Telefone" name="telefone" />
        </div>

        <div class="row">
           <campo:texto id="rua" tipo="text" css="col-md-6" label="Rua:" placeholder="Rua" name="rua" />
           <campo:texto id="bairro" tipo="text"  css="col-md-2" label="Numero:" placeholder="Numero" name="numero" />
           <campo:texto id="bairro" tipo="text" css="col-md-4" label="Bairro:" placeholder="Bairro" name="bairro" />
        </div>
        <div class="row">
           <campo:texto id="cidade" tipo="text" css="col-md-6" label="Cidade:" placeholder="Cidade" name="cidade" />
           <campo:texto id="estado" tipo="text" css="col-md-2" label="Estado:" placeholder="Estado" name="estado" />
           <campo:texto id="pais" tipo="text" css="col-md-4" label="País:" placeholder="País" name="pais" />
        </div>

        <div class="row">
            <button type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i>Salvar</button>
            <button type="button" onclick="history.go(-1)" class="btn"><i class="icon-remove"></i>Cancelar</button>
        </div>
    </form>
</div>

</body>