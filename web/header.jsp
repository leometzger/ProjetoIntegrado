<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="campo" tagdir="/WEB-INF/tags" %>

<html>
<head>
    <title>Projeto Integrado</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="#" class="navbar-brand">Gerenciador de Estoques</a>
        </div>
        <div>
            <ul class="nav navbar-nav">
                <campo:liNavBar label="Fornecedor" urlCriar="/fornecedor/create" urlProcurar="/fornecedor" />
                <campo:liNavBar label="Usuario" urlCriar="/usuario/create" urlProcurar="/usuario" />
                <campo:liNavBar label="Produto" urlCriar="/produto/create" urlProcurar="/produto" />
                <campo:liNavBar label="Estoque" urlCriar="/estoque/create" urlProcurar="/estoque" />
                <li><a href="/relatorios" >Relatorios</a></li>
            </ul>
        </div>
    </div>
</nav>
</html>
