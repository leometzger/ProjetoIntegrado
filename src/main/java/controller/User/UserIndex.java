package controller.User;

import dao.DAOClass;
import dao.UsuarioDAO;
import modelo.Usuario;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static util.Views.INDEX_USUARIO;

@WebServlet(name = "usuario", urlPatterns = "/usuario")
public class UserIndex extends HttpServlet {

    private DAOClass dao;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            dao = new UsuarioDAO();
            List<Usuario> list = dao.retrieveAll();
            req.setAttribute("list",list);
            req.getRequestDispatcher(INDEX_USUARIO).forward(req,resp);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
