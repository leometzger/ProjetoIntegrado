<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<body>
<jsp:include page="../header.jsp" />
<div class="col-md-10 col-md-offset-1">
    <table class="table">
        <thead>
        <tr>
            <td>Contador</td>
            <td>Razão Social</td>
            <td>Nome Fantasia</td>
            <td>CNPJ</td>
            <td>Telefone</td>
            <td>Representante</td>
        </tr>
        </thead>
        <tbody>
    <c:forEach items="${requestScope.list}" varStatus="id" var="item">
        <tr>
            <td>${id.count}</td>
            <td>${item.razaoSocial}</td>
            <td>${item.nomeFantasia}</td>
            <td>${item.cnpj}</td>
            <td>${item.telefone}</td>
            <td>${item.representante}</td>
            <td><a href="/fornecedor/delete?id=${item.id}" class="btn btn-mini btn-danger"><i class="icon-remove icon-white"></i>Delete</a></td>
        </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>