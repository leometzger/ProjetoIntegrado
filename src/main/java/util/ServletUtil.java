package util;

import com.sun.deploy.net.HttpResponse;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by root on 11/15/16.
 */
public class ServletUtil {

    public static void dispatch(HttpServletRequest request, HttpServletResponse response, String URL){
        try {
            request.getRequestDispatcher(URL).forward(request,response);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }
}
