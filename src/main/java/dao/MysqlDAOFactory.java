package dao;

public class MysqlDAOFactory implements DAOFactory {

    @Override
    public DAOClass makeEnderecoDAO() throws ClassNotFoundException {
        return new EnderecoDAO();
    }

    @Override
    public DAOClass makeEstoqueDAO() throws ClassNotFoundException {
        return new EstoqueDAO();
    }

    @Override
    public DAOClass makeFornecedorDAO() throws ClassNotFoundException {
        return new FornecedorDAO();
    }

    @Override
    public DAOClass makeMarcaDAO() throws ClassNotFoundException {
        return new MarcaDAO();
    }

    @Override
    public DAOClass makeProdutoDAO() throws ClassNotFoundException {
        return new ProdutoDAO();
    }

    @Override
    public DAOClass makeUsuarioDAO() throws ClassNotFoundException {
        return new UsuarioDAO();
    }

    @Override
    public DAOClass makeProdutoEstoqueDAO() throws ClassNotFoundException {
        return new ProdutoEstoqueDAO();
    }
}
