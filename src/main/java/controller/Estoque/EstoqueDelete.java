package controller.Estoque;

import dao.DAOClass;
import dao.DAOFactory;
import dao.MysqlDAOFactory;
import util.ServletUtil;
import util.Views;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "deleteEstoque",urlPatterns = "/estoque/delete")
public class EstoqueDelete extends HttpServlet {

    private DAOFactory factory;
    private DAOClass dao;

    public EstoqueDelete() throws ClassNotFoundException {
        factory = new MysqlDAOFactory();
        dao = factory.makeEstoqueDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.parseLong(req.getParameter("id"));
        dao.delete(id);
        resp.sendRedirect("/estoque");
    }
}
