<%@attribute name="id" %>
<%@attribute name="name" %>
<%@attribute name="label" %>
<%@attribute name="placeholder" %>
<%@attribute name="css" %>
<%@attribute name="tipo" %>
<%@attribute name="valor" %>

<div class="form-group ${css}">
    <label for="${id}">${label}</label>
    <input type="${tipo}" value="${valor}" required="true" class="form-control" name="${name}" id="${id}" placeholder="${placeholder}">
</div>