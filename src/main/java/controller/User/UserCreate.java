package controller.User;

import dao.DAOClass;
import dao.DAOFactory;
import dao.MysqlDAOFactory;
import modelo.Usuario;
import util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static util.Views.CRIA_USUARIO;
import static util.Views.INDEX_USUARIO;

@WebServlet(name = "createUser", urlPatterns = "/usuario/create")
public class UserCreate extends HttpServlet {

    private DAOFactory factory;
    private DAOClass dao;

    public UserCreate() throws ClassNotFoundException {
        factory = new MysqlDAOFactory();
        dao = factory.makeUsuarioDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //Usuario u = (Usuario) req.getSession().getAttribute("usuario");
        //if(u.getPrivilegios() < 4)
          //  ServletUtil.dispatch(req, resp, INDEX_USUARIO);
            //resp.sendError();
        ServletUtil.dispatch(req, resp, CRIA_USUARIO);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        Usuario u = new Usuario();
        u.setNome(req.getParameter("nome"));
        u.setPassword(req.getParameter("senha"));
        u.setTelefone(req.getParameter("telefone"));
        u.setCpf(req.getParameter("cpf"));
        dao.create(u);
        ServletUtil.dispatch(req, resp, "/usuario");
    }
}
