package controller.User;

import dao.DAOClass;
import dao.DAOFactory;
import dao.MysqlDAOFactory;
import dao.UsuarioDAO;
import util.ServletUtil;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "deleteUsuario", urlPatterns = "/usuario/delete")
public class UserDelete extends HttpServlet {

    private DAOFactory factory;
    private DAOClass dao;

    public UserDelete() throws ClassNotFoundException {
        factory = new MysqlDAOFactory();
        dao = factory.makeUsuarioDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        Long id = Long.parseLong(req.getParameter("id"));
        dao.delete(id);
        ServletUtil.dispatch(req,resp,"/usuario");
    }
}
