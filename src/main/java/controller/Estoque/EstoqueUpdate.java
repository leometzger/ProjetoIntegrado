package controller.Estoque;

import dao.DAOClass;
import dao.DAOFactory;
import dao.MysqlDAOFactory;
import modelo.Estoque;
import util.ServletUtil;
import util.Views;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.View;
import java.io.IOException;

@WebServlet(name = "updateEstoque",urlPatterns = "/estoque/update")
public class EstoqueUpdate extends HttpServlet {

    private DAOFactory factory;
    private DAOClass dao;

    public EstoqueUpdate() throws ClassNotFoundException {
        factory = new MysqlDAOFactory();
        dao = factory.makeEstoqueDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        Long id = Long.parseLong(req.getParameter("id"));
        Estoque estoque = (Estoque) dao.retrieve(id);
        req.setAttribute("estoque", estoque);
        ServletUtil.dispatch(req,resp, Views.UPDATE_ESTOQUE);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {

        try {
            System.out.println(req.getParameter("id"));
            Long id = Long.parseLong(req.getParameter("id"));
            Estoque e = (Estoque) dao.retrieve(id);
            e.setLocalizacao(req.getParameter("localizacao"));
            dao.update(e);
            resp.sendRedirect("/estoque");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
