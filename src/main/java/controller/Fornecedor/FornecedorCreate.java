package controller.Fornecedor;

import dao.DAOClass;
import dao.DAOFactory;
import dao.MysqlDAOFactory;
import modelo.Endereco;
import modelo.Fornecedor;
import util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static util.Views.CRIA_FORNECEDOR;

@WebServlet(name = "createFornecedor", urlPatterns = "/fornecedor/create")
public class FornecedorCreate extends HttpServlet {

    private DAOFactory factory;
    private DAOClass dao;
    private DAOClass daoEndereco;

    public FornecedorCreate() throws ClassNotFoundException {
        factory = new MysqlDAOFactory();
        dao = factory.makeFornecedorDAO();
        daoEndereco = factory.makeEnderecoDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp){
        ServletUtil.dispatch(req, resp, CRIA_FORNECEDOR);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        Endereco e = new Endereco();
        e.setNumero(Integer.parseInt(req.getParameter("numero")));
        e.setBairro(req.getParameter("bairro"));
        e.setRua(req.getParameter("rua"));
        e.setCidade(req.getParameter("cidade"));
        e.setPais(req.getParameter("pais"));
        e.setEstado(req.getParameter("estado"));
        e = (Endereco) daoEndereco.create(e);

        Fornecedor f = new Fornecedor();
        f.setEndereco(e);
        f.setNomeFantasia(req.getParameter("nomeFantasia"));
        f.setRepresentante(req.getParameter("representante"));
        f.setCnpj(req.getParameter("cnpj"));
        f.setTelefone(req.getParameter("telefone"));
        f.setRazaoSocial(req.getParameter("razaoSocial"));

        dao.create(f);
        resp.sendRedirect("/fornecedor");
   }
}
