import dao.DAOFactory;
import dao.MysqlDAOFactory;

/**
 * Created by root on 11/21/16.
 */
public class Configuracoes {
    private static Configuracoes ourInstance = new Configuracoes();
    private static DAOFactory factory;

    public static Configuracoes getInstance() {
        return ourInstance;
    }

    private Configuracoes() {
        // Aqui seria um factory method se tivesse mais de uma daofactory
        this.factory = new MysqlDAOFactory();
    }

    public DAOFactory getFactory() {
        return this.getFactory();
    }
}
