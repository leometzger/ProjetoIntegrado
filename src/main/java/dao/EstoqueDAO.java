package dao;

import modelo.Estoque;
import modelo.Produto;

import java.sql.ResultSet;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class EstoqueDAO extends MysqlDAO<Estoque> implements DAOClass<Estoque, Long> {

    public EstoqueDAO() throws ClassNotFoundException {
        super();
        selectALLSQL = "SELECT idEstoque, local FROM Estoque";
        selectSQL = "SELECT idEstoque, local FROM Estoque WHERE idEstoque = ?";
        updateSQL = "UPDATE Estoque SET local = ? WHERE idEstoque = ?";
        insertSQL = "INSERT INTO Estoque (local) VALUES (?)";
        deleteSQL = "DELETE FROM Estoque WHERE idEstoque = ?";
        idNumberUpdate = 2;
    }

    @Override
    protected Estoque createObject(ResultSet rs) throws SQLException {
        Estoque e = new Estoque();
        e.setId(rs.getLong("idEstoque"));
        e.setLocalizacao(rs.getString("local"));
        return e;
    }

    @Override
    protected void configStatement(Estoque object) throws SQLException {
        statement.setString(1, object.getLocalizacao());
        statement.setLong(2, object.getId());
    }

}
