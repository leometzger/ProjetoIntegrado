package dao;

import modelo.ModelClass;

import java.sql.*;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by leometzger on 11/7/16.
 */
public abstract class MysqlDAO<E extends ModelClass> implements DAOClass<E,Long> {

    protected Connection conn = null;
    protected PreparedStatement statement = null;
    protected String updateSQL;
    protected String insertSQL;
    protected String selectSQL;
    protected String selectALLSQL;
    protected String deleteSQL;
    protected int idNumberUpdate;

    public MysqlDAO() throws ClassNotFoundException {
       Class.forName("com.mysql.jdbc.Driver");
    }

    protected abstract void configStatement(E object) throws SQLException;
    protected abstract E createObject(ResultSet rs) throws SQLException, ClassNotFoundException;

    protected Connection getConnection() throws SQLException {
        String url = "jdbc:mysql://localhost/stock?autoReconnect=true&useSSL=false";
        Connection conn = DriverManager.getConnection(url,"root","1234");
        return conn;
    }

    protected void closeConnections(Statement stmt, Connection conn) {
        try {
            if(stmt != null)
            stmt.close();
            if(conn != null)
                conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected Long getGeneratedKey() throws SQLException {
        Long id;
        try (ResultSet generatedKeys = this.statement.getGeneratedKeys()) {
            if (generatedKeys.next())
                id = generatedKeys.getLong(1);
            else
                throw new SQLException("Creating user failed, no ID obtained.");
        }
        return id;
    }

    public ResultSet query(String sql) throws SQLException {
        conn = getConnection();
        statement = conn.prepareStatement(sql);
        ResultSet rs = statement.executeQuery();
        return rs;
    }

    public ResultSet query(String sql,Long id) throws SQLException {
        conn = getConnection();
        statement = conn.prepareStatement(sql);
        statement.setLong(1, id);
        ResultSet rs = statement.executeQuery();
        return rs;
    }

    public ResultSet query(String sql,String param) throws SQLException {
        conn = getConnection();
        statement = conn.prepareStatement(sql);
        statement.setString(1, param);
        ResultSet rs = statement.executeQuery();
        return rs;
    }

    public boolean execute(String sql, Long id) throws SQLException {
        conn = getConnection();
        statement = conn.prepareStatement(sql);
        statement.setLong(1, id);
        boolean rs = statement.execute();
        return rs;
    }


    public Long insert(String sql, E obj){
        Long id = null;
        try {
            conn = this.getConnection();
            statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            this.configStatement(obj);
            statement.executeUpdate();
            id = this.getGeneratedKey();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public E create(E object) {
        String sql = insertSQL;
        try {
            Long id = this.insert(sql, object);
            object.setId(this.getGeneratedKey());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return object;
    }

    @Override
    public E retrieve(Long id) {
        E object = null;
        String sql = selectSQL;
        try {
            ResultSet rs = this.query(sql,id);
            if (rs.next())
                object= this.createObject(rs);
        }catch (SQLException e){
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return object;
    }

    @Override
    public List<E> retrieveAll() {
        String sql = selectALLSQL;
        List list = new ArrayList<E>();
        try {
            ResultSet rs = this.query(sql);
            while (rs.next())
                list.add(this.createObject(rs));
        }catch (SQLException e){
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public E update(E object) {
        String sql = updateSQL;
        try {
            conn = this.getConnection();
            statement = conn.prepareStatement(sql);
            configStatement(object);
            statement.setLong(idNumberUpdate, object.getId());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return object;
    }

    @Override
    public E delete(Long id) {
        String sql = deleteSQL;
        try {
            this.execute(sql,id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public E delete(E object) {
        this.delete(object.getId());
        return null;
    }
}