package dao;

import modelo.Endereco;
import modelo.Fornecedor;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by leometzger on 10/10/16.
 */
public class FornecedorDAO extends MysqlDAO<Fornecedor> {

    public FornecedorDAO() throws ClassNotFoundException {
        super();
        selectALLSQL =  "SELECT idFornecedor,razaoSocial,cnpj,nomeFantasia," +
                        "representante,telefone,Endereco_idEndereco" +
                        " FROM Fornecedor";

        selectSQL = "SELECT idFornecedor,razaoSocial,cnpj,nomeFantasia," +
                    "representante,telefone,Endereco_idEndereco" +
                    " FROM Fornecedor WHERE idFornecedor = ?";

        updateSQL = "UPDATE Fornecedor SET razaoSocial = ?, cnpj = ?, nomeFantasia = ?," +
                    "representante = ?, telefone = ?, Endereco_idEndereco = ? " +
                    "WHERE idFornecedor = ?";

        insertSQL = "INSERT INTO Fornecedor (razaoSocial,cnpj,nomeFantasia," +
                    "representante,telefone,Endereco_idEndereco) " +
                    "VALUES (?,?,?,?,?,?)";

        deleteSQL = "DELETE FROM Fornecedor WHERE idFornecedor = ?";

        idNumberUpdate = 7;
    }

    @Override
    protected void configStatement(Fornecedor object) throws SQLException {
        statement.setString(1,object.getRazaoSocial());
        statement.setString(2,object.getCnpj());
        statement.setString(3,object.getNomeFantasia());
        statement.setString(4,object.getRepresentante());
        statement.setString(5,object.getTelefone());
        statement.setLong(6,object.getEndereco().getId());
    }

    @Override
    protected Fornecedor createObject(ResultSet rs) throws SQLException, ClassNotFoundException {
        Endereco e = new EnderecoDAO().retrieve(rs.getLong("Endereco_idEndereco"));
        Fornecedor fornecedor = new Fornecedor();
        fornecedor.setId(rs.getLong("idFornecedor"));
        fornecedor.setRazaoSocial(rs.getString("razaoSocial"));
        fornecedor.setCnpj(rs.getString("cnpj"));
        fornecedor.setNomeFantasia(rs.getString("nomeFantasia"));
        fornecedor.setRepresentante(rs.getString("representante"));
        fornecedor.setTelefone(rs.getString("telefone"));
        fornecedor.setEndereco(e);
        return fornecedor;
    }
}
