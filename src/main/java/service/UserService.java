package service;

import dao.UsuarioDAO;
import modelo.Usuario;

public class UserService {

    private UsuarioDAO dao;

    public Usuario autentica(String username, String password){
        Usuario u = null;
        try {
            if(dao == null)
                dao = new UsuarioDAO();
            u = dao.findByNameAndPassword(username, password);
        } catch (ClassNotFoundException e) {
            System.out.println("deu treta Service.");
        }
        return u;
    }

}
