package controller.User;

import dao.DAOClass;
import modelo.Usuario;
import util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import static util.Views.UPDATE_USUARIO;

@WebServlet(name = "updateUsuario", urlPatterns = "/usuario/update")
public class UserUpdate extends HttpServlet {

    private DAOClass dao;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Usuario usuario = (Usuario) dao.retrieve(req.getParameter("id"));
        req.setAttribute("usuario", usuario);
        ServletUtil.dispatch(req, resp, UPDATE_USUARIO);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.parseLong(req.getParameter("id"));
        Usuario u = (Usuario) dao.retrieve(id);
        u.setNome(req.getParameter("nome"));
        u.setPassword(req.getParameter("senha"));
        u.setTelefone(req.getParameter("telefone"));
        u.setCpf(req.getParameter("cpf"));
        dao.update(u);
        resp.sendRedirect("/usuario");

    }
}
