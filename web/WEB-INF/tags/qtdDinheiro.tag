<%@attribute name="id" %>
<%@attribute name="name" %>
<%@attribute name="label" %>
<%@attribute name="css" %>
<%@attribute name="valor" %>

<div class="form-group ${css}">
    <label class="sr-only" for="${id}">${label}</label>
    <div class="input-group">
        <div class="input-group-addon">$</div>
        <input type="text" value="${valor}" id="${id}" name="${name}" required="true" class="form-control" placeholder="R$">
    </div>
</div>