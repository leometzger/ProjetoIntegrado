<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Projeto Integrado</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>
<div class="row">
    <div class="col-md-4 col col-md-offset-4">
        <form class="form-signin" method="post">
            <h2 class="form-signin-heading">Login</h2>
            <input type="text" class="form-control" name="username" placeholder="Email Address"  />
            <input type="password" class="form-control" name="password" placeholder="Password" />
            <button  class="btn btn-md btn-success btn-block col-md-2 col-md-offset-1" type="submit">Login</button>
        </form>
    </div>
</div>
</body>
</html>
