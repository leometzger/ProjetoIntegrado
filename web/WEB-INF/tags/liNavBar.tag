<%@attribute name="urlCriar" %>
<%@attribute name="urlRelatorios" %>
<%@attribute name="urlProcurar" %>
<%@attribute name="label" %>

<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        ${label}
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
        <li><a href="${urlCriar}">Criar</a></li>
        <li><a href="${urlProcurar}">Procurar</a></li>
    </ul>
</li>