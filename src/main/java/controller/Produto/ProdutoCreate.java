package controller.Produto;

import dao.DAOClass;
import dao.DAOFactory;
import dao.MysqlDAO;
import dao.MysqlDAOFactory;
import modelo.*;
import util.ServletUtil;
import util.Views;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static util.Views.CRIA_PRODUTO;
import static util.Views.INDEX_PRODUTO;

@WebServlet(name = "createProduto", urlPatterns = "/produto/create")
public class ProdutoCreate extends HttpServlet {

    private DAOFactory factory;
    private DAOClass dao;
    private DAOClass fornDao;
    private DAOClass marcaDAO;
    private DAOClass estoqueDAO;
    private DAOClass estoqueProdutoDAO;

    public ProdutoCreate() throws ClassNotFoundException {
        factory = new MysqlDAOFactory();
        dao = factory.makeProdutoDAO();
        fornDao = factory.makeFornecedorDAO();
        marcaDAO = factory.makeMarcaDAO();
        estoqueDAO = factory.makeEstoqueDAO();
        estoqueProdutoDAO = factory.makeProdutoEstoqueDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
         List<Fornecedor> fornecedores = fornDao.retrieveAll();
         req.setAttribute("fornecedores", fornecedores);

         List<Marca> marcas = marcaDAO.retrieveAll();
         req.setAttribute("marcas",marcas);

         List<Estoque> estoques = estoqueDAO.retrieveAll();
         req.setAttribute("estoques", estoques);

         ServletUtil.dispatch(req, resp, CRIA_PRODUTO);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        System.out.println(req.getParameter("marca"));

        Long fornecedor = Long.parseLong(req.getParameter("fornecedor"));
        Fornecedor f = (Fornecedor) fornDao.retrieve(fornecedor);

        Long estoque = Long.parseLong(req.getParameter("estoque"));
        Estoque e = (Estoque) estoqueDAO.retrieve(estoque);

        Long marca = Long.parseLong(req.getParameter("marca"));
        Marca m = (Marca) marcaDAO.retrieve(marca);

        Produto p = new Produto();
        p.setNome(req.getParameter("nome"));
        p.setCor(req.getParameter("cor"));
        p.setPrecoVenda(Double.parseDouble(req.getParameter("precoVenda")));
        p.setPrecoCompra(Double.parseDouble(req.getParameter("precoCompra")));
        p.setTipo(req.getParameter("tipo"));
        p.setEstoque(e);
        p.setFornecedor(f);
        p.setMarca(m);

        dao.create(p);

        ProdutoEstoque produtoEstoque = new ProdutoEstoque();
        produtoEstoque.setEstoque(e);
        produtoEstoque.setProduto(p);
        produtoEstoque.setQuantidade(Integer.parseInt(req.getParameter("quantidade")));
        estoqueProdutoDAO.create(produtoEstoque);

        resp.sendRedirect("/produto");
    }
}
