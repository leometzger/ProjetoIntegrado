<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../header.jsp" />

<div class="col-md-10 col-md-offset-1">
    <table class="table">
        <thead>
        <tr>
            <td>Contador</td>
            <td>Nome</td>
            <td>CPF</td>
            <td>Nascimento</td>
            <td>Telefone</td>
        </tr>
        </thead>
        <tbody>
    <c:forEach items="${requestScope.list}" varStatus="id" var="item">
        <tr>
            <td>${id.count}</td>
            <td>${item.nome}</td>
            <td>${item.cpf}</td>
            <td>${item.nascimento}</td>
            <td>${item.telefone}</td>
            <td><a href="/usuario/update?id=${item.id}" class="btn btn-mini btn-primary"><i class="icon-edit icon-white"></i>Edit</a></td>
            <td><a href="/usuario/delete?id=${item.id}"  class="btn btn-mini btn-danger"><i class="icon-remove icon-white"></i>Delete</a></td>
        </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
