package controller.Fornecedor;

import dao.DAOClass;
import dao.DAOFactory;
import dao.FornecedorDAO;
import dao.MysqlDAOFactory;
import modelo.Fornecedor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import static util.Views.INDEX_FORNECEDOR;


@WebServlet(name = "fornecedor", urlPatterns = "/fornecedor")
public class FornecedorIndex extends HttpServlet {

    private DAOFactory factory;
    private DAOClass dao;

    public FornecedorIndex() throws ClassNotFoundException {
        factory = new MysqlDAOFactory();
        dao = factory.makeFornecedorDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         List<Fornecedor> list = dao.retrieveAll();
         req.setAttribute("list",list);
         req.getRequestDispatcher(INDEX_FORNECEDOR).forward(req,resp);
    }
}
