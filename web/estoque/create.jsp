<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="campo" tagdir="/WEB-INF/tags" %>

<body>
<jsp:include page="../header.jsp" />

<div class="row">
<div class="col-md-8 col-md-offset-2">
    <h2>Criando um Estoque</h2>
</div>
</div>
    <form class="col-md-8 col-md-offset-2" method="post" action="/estoque/create">
        <div class="row">
            <campo:texto label="Localização:" name="localizacao" placeholder="Localização" id="localizacao" />
        </div>
        <div>
            <button type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i>Salvar</button>
            <button type="button" onclick="history.go(-1)" class="btn"><i class="icon-remove"></i>Cancelar</button>
        </div>
    </form>
</div>
</body>

