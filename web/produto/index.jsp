<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="campo" tagdir="/WEB-INF/tags" %>
<body>
<jsp:include page="../header.jsp" />

<div class="col-md-10 col-md-offset-1">
    <form method="post" action="/produto">
    <div class="row">
    <campo:texto id="procura" css="col-md-8 col-md-offset-2"  name="procura" tipo="text" label="Procurar" placeholder="Digite o nome que quer procurar" />
    </div>
    <button type="submit" id="procurar" class="btn btn-primary col-md-offset-2" href="">Procurar</button>
    </form>
    <table class="table">
        <thead>
        <tr>
            <td>ID</td>
            <td>Produto</td>
            <td>Preço Compra</td>
            <td>Preço Venda</td>
            <td>Cor</td>
            <td>Tipo</td>
        </tr>
        </thead>
        <tbody>
    <c:forEach items="${requestScope.list}" varStatus="id" var="item">
        <tr>
            <td>${id.count}</td>
            <td>${item.nome}</td>
            <td>${item.precoCompra}</td>
            <td>${item.precoVenda}</td>
            <td>${item.cor}</td>
            <td>${item.tipo}</td>
            <td><a href="/produto/update?id=${item.id}" class="btn btn-mini btn-primary"><i class="icon-edit icon-white"></i>Editar</a></td>
            <td><a href="/produto/delete?id=${item.id}" class="btn btn-mini btn-danger"><i class="icon-remove icon-white"></i>Deletar</a></td>
        </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>