package controller.Produto;

import dao.*;
import modelo.Estoque;
import modelo.Produto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static util.Views.INDEX_PRODUTO;


@WebServlet(name = "produto", urlPatterns = "/produto")
public class ProdutoIndex extends HttpServlet{

    private DAOFactory factory;
    private ProdutoDAO dao;

    public ProdutoIndex() throws ClassNotFoundException {
        factory = new MysqlDAOFactory();
        dao = (ProdutoDAO) factory.makeProdutoDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Produto> list = null;
        System.out.println(req.getParameter("procura"));
        if(req.getAttribute("procura") != null){
            try {
                String procura = (String) req.getParameter("procura");
                list = dao.findByName(procura);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        else
            list = dao.retrieveAll();
        System.out.print("aqui passa");
        req.setAttribute("list", list);
        req.getRequestDispatcher(INDEX_PRODUTO).forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String procura = req.getParameter("procura");
        req.setAttribute("procura",procura);
        doGet(req,resp);
    }
}
