package modelo;

/**
 * Created by root on 11/19/16.
 */
public class ProdutoEstoque implements ModelClass {

    private Estoque estoque;
    private Produto produto;
    private int quantidade;

    public Long getId() {
        return produto.getId();
    }

    public void setId(Long id) {
        produto.setId(id);
    }

    public Estoque getEstoque() {
        return estoque;
    }

    public void setEstoque(Estoque estoque) {
        this.estoque = estoque;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
