package dao;

import modelo.Estoque;
import modelo.Fornecedor;
import modelo.Marca;
import modelo.Produto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProdutoDAO extends MysqlDAO<Produto>{

    public ProdutoDAO() throws ClassNotFoundException {
        super();

        selectSQL = "SELECT idProduto,descricaoProduto,precoCompra,precoVenda," +
                    "marca_idMarca,cor,tipo,fornecedor_idFornecedor, estoque_idEstoque" +
                    " FROM Produto WHERE idProduto = ?";

        selectALLSQL = "SELECT idProduto,descricaoProduto,precoCompra,precoVenda," +
                        "marca_idMarca,cor,tipo,fornecedor_idFornecedor, estoque_idEstoque" +
                        " FROM Produto";

        insertSQL = "INSERT INTO Produto (descricaoProduto,precoCompra,precoVenda," +
                    "Marca_idMarca,Cor,tipo, fornecedor_idFornecedor, estoque_idEstoque) " +
                    "VALUES (?,?,?,?,?,?,?,?)";
        updateSQL = "UPDATE Produto SET descricaoProduto = ?, precoCompra = ?," +
                    "precoVenda = ?, marca_idMarca = ?, cor = ?, tipo = ?, fornecedor_idFornecedor = ?," +
                    "estoque_idEstoque = ? " +
                    "WHERE idProduto = ?";
        deleteSQL = "DELETE FROM Produto WHERE idProduto = ?";
        idNumberUpdate = 9;
    }

    @Override
    protected void configStatement(Produto produto) throws SQLException {
        statement.setString(1, produto.getNome());
        statement.setDouble(2, produto.getPrecoCompra());
        statement.setDouble(3, produto.getPrecoVenda());
        statement.setLong(  4, produto.getMarca().getId());
        statement.setString(5, produto.getCor());
        statement.setString(6, produto.getTipo());
        statement.setLong(  7, produto.getFornecedor().getId());
        statement.setLong(  8, produto.getEstoque().getId());
    }


    /**
     *
     * @param rs : resultSet referente a consulta feita no banco.
     * @return uma instancia do objeto genérico.
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    protected Produto createObject(ResultSet rs) throws SQLException, ClassNotFoundException {
        Long idMarca = rs.getLong("Marca_idMarca");
        Long idFornecedor = rs.getLong("Fornecedor_idFornecedor");
        Long idEstoque = rs.getLong("Estoque_idEstoque");
        Marca m;
        Fornecedor f;
        Estoque e;

        Produto produto = new Produto();
        produto.setId(rs.getLong("idProduto"));
        produto.setNome(rs.getString("descricaoProduto"));
        produto.setCor(rs.getString("cor"));
        produto.setTipo(rs.getString("tipo"));
        produto.setPrecoCompra(rs.getDouble("precoCompra"));
        produto.setPrecoVenda(rs.getDouble("precoVenda"));

        MysqlDAOFactory factory = new MysqlDAOFactory();

        m = idMarca != null ? (Marca) factory.makeMarcaDAO().retrieve(idMarca) : null;
        f = idFornecedor != null ? (Fornecedor) factory.makeFornecedorDAO().retrieve(idFornecedor) : null;
        e = idEstoque != null ? (Estoque) factory.makeFornecedorDAO().retrieve(idEstoque) : null;

        produto.setMarca(m);
        produto.setFornecedor(f);
        produto.setEstoque(e);

        return produto;
    }

    public List<Produto> findByEstoque(Long id) throws SQLException, ClassNotFoundException {
        List<Produto> list = new ArrayList<>();
        String sql = this.selectALLSQL + " WHERE Estoque_idEstoque = ?";
        ResultSet rs = query(sql, id);
        while (rs.next())
            list.add(this.createObject(rs));
        return list;
    }

    /**
     *
     * @param nome: nome usado para fazer a consulta.
     * @return uma lista referente ao resultado da consulta.
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public List<Produto> findByName(String nome) throws SQLException, ClassNotFoundException {
        List<Produto> list = new ArrayList<>();
        String sql = this.selectALLSQL + " WHERE descricaoProduto = ?";
        ResultSet rs = query(sql, nome);
        while (rs.next())
            list.add(this.createObject(rs));
        return list;
     }
}