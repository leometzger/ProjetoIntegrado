package dao;

/**
 * Created by root on 11/17/16.
 */
public interface DAOFactory {

    public DAOClass makeEnderecoDAO() throws ClassNotFoundException;
    public DAOClass makeEstoqueDAO() throws ClassNotFoundException;
    public DAOClass makeFornecedorDAO() throws ClassNotFoundException;
    public DAOClass makeMarcaDAO() throws ClassNotFoundException;
    public DAOClass makeProdutoDAO() throws ClassNotFoundException;
    public DAOClass makeUsuarioDAO() throws ClassNotFoundException;
    public DAOClass makeProdutoEstoqueDAO() throws ClassNotFoundException;

}
