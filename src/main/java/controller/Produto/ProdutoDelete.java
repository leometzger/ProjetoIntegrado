package controller.Produto;

import dao.DAOClass;
import dao.DAOFactory;
import dao.MysqlDAOFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "deleteProduto", urlPatterns = "/produto/delete")
public class ProdutoDelete extends HttpServlet {

    private DAOFactory factory;
    private DAOClass dao;

    public ProdutoDelete() throws ClassNotFoundException {
        factory = new MysqlDAOFactory();
        dao = factory.makeProdutoDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.parseLong(req.getParameter("id"));
        dao.delete(id);
        resp.sendRedirect("/produto");
    }
}
