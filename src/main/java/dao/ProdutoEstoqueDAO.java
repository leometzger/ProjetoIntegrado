package dao;

import modelo.Estoque;
import modelo.Produto;
import modelo.ProdutoEstoque;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ProdutoEstoqueDAO extends MysqlDAO<ProdutoEstoque> {


    public ProdutoEstoqueDAO() throws ClassNotFoundException {
        selectALLSQL = "SELECT produto_idProduto, estoque_idEstoque,quantidade_prod FROM Produto_Estoque";
        updateSQL = "UPDATE Produto_Estoque SET produto_idProduto = ?, estoque_idEstoque = ?, quantidade_prod = ? " +
                "WHERE produto_idProduto = ?";
        insertSQL = "INSERT INTO Produto_Estoque (produto_idProduto, estoque_idEstoque,quantidade_prod) VALUES (?,?,?)";
        deleteSQL = "DELETE FROM Produto_Estoque WHERE produto_idProduto = ?";
        idNumberUpdate = 4;
    }

    @Override
    protected void configStatement(ProdutoEstoque produtoEstoque) throws SQLException {
        statement.setLong(1, produtoEstoque.getProduto().getId());
        statement.setLong(2, produtoEstoque.getEstoque().getId());
        statement.setInt(3, produtoEstoque.getQuantidade());
    }

    @Override
    protected ProdutoEstoque createObject(ResultSet rs) throws SQLException, ClassNotFoundException {

        MysqlDAOFactory factory = new MysqlDAOFactory();
        Long idEstoque = rs.getLong("estoque_idEstoque");
        Long idProduto = rs.getLong("produto_idProduto");

        Estoque e = (Estoque) factory.makeEstoqueDAO().retrieve(idEstoque);
        Produto p = (Produto) factory.makeProdutoDAO().retrieve(idProduto);

        ProdutoEstoque produtoEstoque = new ProdutoEstoque();
        produtoEstoque.setEstoque(e);
        produtoEstoque.setProduto(p);
        produtoEstoque.setQuantidade(rs.getInt("quantidade"));

        return produtoEstoque;
    }

    @Override
    public Long insert(String sql, ProdutoEstoque obj){
        try {
            conn = this.getConnection();
            statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            this.configStatement(obj);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
