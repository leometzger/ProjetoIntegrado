package util;

public class Views {

    public static final String CRIA_PRODUTO = "/produto/create.jsp";

    public static final String UPDATE_PRODUTO = "/produto/update.jsp";

    public static final String INDEX_PRODUTO = "/produto/index.jsp";

    public static final String CRIA_ESTOQUE = "/estoque/create.jsp";

    public static final String INDEX_ESTOQUE = "/estoque/index.jsp";

    public static final String UPDATE_ESTOQUE = "/estoque/update.jsp";

    public static final String CRIA_FORNECEDOR = "/fornecedor/create.jsp";

    public static final String INDEX_FORNECEDOR = "/fornecedor/index.jsp";

    public static final String CRIA_USUARIO = "/usuario/create.jsp";

    public static final String INDEX_USUARIO = "/usuario/index.jsp";
    public static final String UPDATE_USUARIO = "/usuario/update.jsp";
    public static final String RELATORIOS= "/Relatorios.jsp";
}
