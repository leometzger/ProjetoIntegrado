package dao;

import modelo.Marca;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by root on 11/14/16.
 */
public class MarcaDAO extends MysqlDAO<Marca> {

    public MarcaDAO() throws ClassNotFoundException {
        super();
        selectALLSQL = "Select idMarca, nomeMarca, ranking From Marca";
        selectSQL = "Select idMarca, nomeMarca, ranking From Marca Where idMarca = ?";
        deleteSQL = "Delete from Marca where idMarca = ?";
        updateSQL = "UPDATE Marca SET nomeMarca = ?, ranking = ? Where idMarca = ?";
        insertSQL = "Insert into Marca (nomeMarca,ranking) VALUES (?,?)";
        idNumberUpdate = 3;
    }

    @Override
    protected void configStatement(Marca marca) throws SQLException {
        statement.setString(1, marca.getNome());
        statement.setInt(2,marca.getRanking());
    }

    @Override
    protected Marca createObject(ResultSet rs) throws SQLException, ClassNotFoundException {
        Marca m = new Marca();
        m.setId(rs.getLong("idMarca"));
        m.setNome(rs.getString("nomeMarca"));
        m.setRanking(rs.getInt("ranking"));
        return m;
    }
}
