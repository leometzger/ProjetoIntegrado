package controller.Fornecedor;

import dao.DAOClass;
import dao.DAOFactory;
import dao.FornecedorDAO;
import dao.MysqlDAOFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "deleteFornecedor", urlPatterns = "/fornecedor/delete")
public class FornecedorDelete extends HttpServlet {

    private DAOFactory factory;
    private DAOClass dao;

    public FornecedorDelete() throws ClassNotFoundException {
        factory = new MysqlDAOFactory();
        dao = factory.makeFornecedorDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.parseLong(req.getParameter("id"));
        System.out.println(id);
        dao.delete(id);
        resp.sendRedirect("/fornecedor");
    }
}
