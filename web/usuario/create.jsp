<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="campo" tagdir="/WEB-INF/tags" %>

<body>
<jsp:include page="../header.jsp" />


<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h2>Criando um novo Usuario</h2>
    </div>
    <div>
        <form class="col-md-6 col-md-offset-3" method="post" action="/usuario/create">
            <div class="row" >
                <campo:texto tipo="text" label="Nome" id="nome" placeholder="Nome" name="nome" />
                <campo:texto tipo="text" label="CPF:" id="cpf" placeholder="CPF" name="cpf" />
            </div>

            <div class="row" >
                <campo:texto tipo="password" label="Senha:" id="senha" placeholder="Senha" name="senha" />
            </div>

            <div class="row" >
                <campo:texto tipo="text" label="Nascimento:" id="nascimento" placeholder="Nascimento" name="nascimento" />
                <campo:texto tipo="text" label="Privilegios" id="privilegios" placeholder="Privilegio (numero)" name="privilegio" />
            </div>

            <div class="row" >
                <campo:texto label="Telefone:" tipo="tel" id="telefone" placeholder="(47) 3333-3333" name="telefone" />
            </div>
            <div>
                <button type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i>Salvar</button>
                <button type="button" onclick="history.go(-1)" class="btn"><i class="icon-remove"></i>Cancelar</button>
            </div>
        </form>
    </div>
</div>
</body>

