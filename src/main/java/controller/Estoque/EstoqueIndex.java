package controller.Estoque;

import dao.DAOClass;
import dao.DAOFactory;
import dao.EstoqueDAO;
import dao.MysqlDAOFactory;
import modelo.Estoque;
import util.ServletUtil;
import util.Views;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static util.Views.INDEX_ESTOQUE;

@WebServlet(name = "estoque", urlPatterns = "/estoque")
public class EstoqueIndex extends HttpServlet {

    private DAOFactory factory;
    private DAOClass dao;

    public EstoqueIndex() throws ClassNotFoundException {
        factory = new MysqlDAOFactory();
        dao = factory.makeEstoqueDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        List<Estoque> list = dao.retrieveAll();
        req.setAttribute("list", list);
        ServletUtil.dispatch(req, resp, INDEX_ESTOQUE);
    }
}
