Projeto final das matérias Programação III e Banco de Dados. 2016-2.

Requisitos:
* Ser construído utilizando JAVA e MYSQL, sem o auxílio de nenhum framework;
* Utilização de pelo menos 3 padrões de projeto;
* 3 ou mais telas distintas;
* 2 ou mais relatórios.
