package controller.Estoque;

import dao.DAOClass;
import dao.DAOFactory;
import dao.MysqlDAOFactory;
import modelo.Estoque;
import util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import static util.Views.CRIA_ESTOQUE;

@WebServlet(name = "criarEstoque",urlPatterns = "/estoque/create")
public class EstoqueCreate extends HttpServlet {

    private DAOFactory factory;
    private DAOClass dao;

    public EstoqueCreate() throws ClassNotFoundException {
        factory = new MysqlDAOFactory();
        dao = factory.makeEstoqueDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        ServletUtil.dispatch(req, resp, CRIA_ESTOQUE);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Estoque e = new Estoque();
        e.setLocalizacao(req.getParameter("localizacao"));
        dao.create(e);
        resp.sendRedirect("/estoque");
    }
}
