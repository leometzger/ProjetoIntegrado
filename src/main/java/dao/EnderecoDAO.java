package dao;

import modelo.Endereco;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EnderecoDAO extends MysqlDAO<Endereco> implements DAOClass<Endereco, Long> {

    public EnderecoDAO() throws ClassNotFoundException {
        super();
        selectALLSQL = "SELECT idEndereco,rua,numero,bairo,estado,cidade,pais FROM Endereco";
        selectSQL = "SELECT idEndereco,rua,numero,bairo,estado,cidade,pais FROM Endereco WHERE idEndereco = ?";
        updateSQL = "UPDATE Endereco SET rua = ?,numero = ?,bairo = ?,estado = ?,cidade = ?,pais = ? " +
                    "WHERE idEndereco = ?";
        insertSQL = "INSERT INTO Endereco (rua,numero,bairo,estado,cidade,pais) VALUES (?,?,?,?,?,?)";
        deleteSQL = "DELETE FROM Endereco WHERE idEndereco = ?";
        idNumberUpdate = 7;
    }

    protected Endereco createObject(ResultSet rs) throws SQLException {
        Endereco e = new Endereco();
        e.setId(rs.getLong("idEndereco"));
        e.setBairro(rs.getString("rua"));
        e.setPais(rs.getString("bairo"));
        e.setEstado(rs.getString("estado"));
        e.setNumero(rs.getInt("numero"));
        e.setPais(rs.getString("pais"));
        e.setCidade(rs.getString("cidade"));
        return e;
    }

    @Override
    protected void configStatement(Endereco endereco) throws SQLException {
        statement.setString(1, endereco.getRua());
        statement.setInt(   2, endereco.getNumero());
        statement.setString(3, endereco.getBairro());
        statement.setString(4, endereco.getEstado());
        statement.setString(5, endereco.getCidade());
        statement.setString(6, endereco.getPais());
    }
}
