package dao;

import modelo.Usuario;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class UsuarioDAO extends MysqlDAO<Usuario> implements DAOClass<Usuario,Long>{

    public UsuarioDAO() throws ClassNotFoundException {
        super();
        selectALLSQL = "SELECT idUsuario,nome,password, cpf, dataNascimento,fone,privilegios FROM Usuario";
        selectSQL = "SELECT idUsuario,nome,password, cpf,dataNascimento,fone,privilegios FROM Usuario WHERE idUsuario = ?";
        updateSQL = "UPDATE Usuario SET nome = ?, cpf = ?, dataNascimento = ?,fone = ?,privilegios = ?, password = ? " +
                    " FROM Usuario WHERE idUsuario = ?";
        insertSQL = "INSERT INTO Usuario (nome,cpf, dataNascimento,fone,privilegios) VALUES (?,?,?,?,?)";
        deleteSQL = "DELETE FROM Usuario WHERE idUsuario = ?";
        idNumberUpdate = 7;
    }

    @Override
    protected void configStatement(Usuario object) throws SQLException {
        statement.setString(1,object.getNome());
        statement.setString(2,object.getCpf());
        statement.setDate(3, object.getNascimento());
        statement.setString(4,object.getTelefone());
        statement.setInt(5, object.getPrivilegios());
    }

    public Usuario findByNameAndPassword(String username, String password){
        Usuario u = null;
        String sql = "SELECT idUsuario,nome,cpf, dataNascimento,fone,privilegios FROM Usuario where nome = ? and password = ?";
        try {
            conn = this.getConnection();
            statement = conn.prepareStatement(sql);
            statement.setString(1, username);
            statement.setString(2, password);
            ResultSet rs = statement.executeQuery();
            System.out.println("depois da query");
            if(rs.next()){
                u = this.createObject(rs);
            }
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            closeConnections(statement, conn);
        }
        return u;
    }

    protected Usuario createObject(ResultSet rs){
        Usuario u = new Usuario();
        try {
            u.setId(rs.getLong("idUsuario"));
            u.setNome(rs.getString("nome"));
            u.setCpf(rs.getString("cpf"));
            u.setNascimento(rs.getDate("dataNascimento"));
            u.setTelefone(rs.getString("fone"));
            u.setPrivilegios(rs.getInt("privilegios"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return u;
    }
}
