<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<body>
<jsp:include page="../header.jsp" />
<div class="col-md-10 col-md-offset-1">
    <table class="table">
        <thead>
        <tr>
            <td width="40%">Contador</td>
            <td width="40%">Local</td>
        </tr>
        </thead>
        <tbody>
    <c:forEach items="${requestScope.list}" varStatus="id" var="item">
        <tr>
            <td>${id.count}</td>
            <td>${item.localizacao}</td>
            <td><a href="/estoque/update?id=${item.id}" class="btn btn-mini btn-primary"><i class="icon-edit icon-white"></i>Editar</a></td>
            <td><a href="/estoque/delete?id=${item.id}" class="btn btn-mini btn-danger"><i class="icon-remove icon-white"></i>Deletar</a></td>
        </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
