<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="campos" tagdir="/WEB-INF/tags" %>

<body>
<jsp:include page="../header.jsp" />
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h2>Atualizando o Produto: ${requestScope.produto.id}</h2>
    </div>
</div>
<div>
    <form class="col-md-8 col-md-offset-2" method="post" action="/produto/update">
        <input id="id" name="id" valor="${requestScope.produto.id}" hidden="true" />

        <campo:texto id="nome" valor="${requestScope.produto.nome}" label="Nome:" placeholder="Nome Produto" name="nome" />
        <campo:texto id="cor" valor="${requestScope.produto.cor}" label="Cor:" placeholder="Cor" name="cor" />
        <campo:texto id="tipo" valor="${requestScope.produto.tipo}" tipo="text" placeholder="Tipo" name="tipo" label="Tipo:" />

        <div class="row">
            <campo:texto css="col-md-2" id="quantidade" name="quantidade" placeholder="Qtd" label="Quantidade:" tipo="text" valor="${requestScope.produtoEstoque.quantidade}"/>
            <campo:qtdDinheiro css="col-md-6" valor="${requestScope.produto.precoCompra}" id="precoCompra" name="precoCompra" label="Preço Compra:" />
            <campo:qtdDinheiro css="col-md-6" valor="${requestScope.produto.precoVenda}"  id="precoVenda" name="precoVenda" label="Preço Venda:" />
        </div>

        <div class="row" >
            <div class="col-md-4">
                <label for="fornecedor">Fornecedor:</label>
                <div>
                    <select id="fornecedor" name="fornecedor" >
                        <c:forEach items="${requestScope.fornecedores}" var="item">
                            <option value="${item.id}" name="fornecedor" >${item.nomeFantasia}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="col-md-4">
                <label for="estoque">Estoque:</label>
                <div>
                    <select id="estoque" name="estoque"  >
                        <c:forEach items="${requestScope.estoques}" var="item">
                            <option value="${item.id}" name="estoque" >${item.localizacao}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <label for="marca">Marca:</label>
                <div>
                    <select id="marca" name="marca"  >
                        <c:forEach items="${requestScope.marcas}"  var="item">
                            <option value="${item.id}" name="marca" >${item.nome}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>

        <div style="margin-top: 50px">
            <button type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i>Salvar</button>
            <button type="button" onclick="history.go(-1)" class="btn"><i class="icon-remove"></i>Cancelar</button>
        </div>
    </form>
</div>
</body>

