package controller.Produto;

import dao.DAOClass;
import dao.DAOFactory;
import dao.MysqlDAOFactory;
import modelo.*;
import util.ServletUtil;
import util.Views;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "updateProduto", urlPatterns = "/produto/update")
public class ProdutoUpdate extends HttpServlet {

    private DAOFactory factory;
    private DAOClass dao;
    private DAOClass fornDao;
    private DAOClass marcaDAO;
    private DAOClass estoqueDAO;
    private DAOClass estoqueProdutoDAO;

    public ProdutoUpdate() throws ClassNotFoundException {
        factory = Configuracoes;
        dao = factory.makeProdutoDAO();
        fornDao = factory.makeFornecedorDAO();
        marcaDAO = factory.makeMarcaDAO();
        estoqueDAO = factory.makeEstoqueDAO();
        estoqueProdutoDAO = factory.makeProdutoEstoqueDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long id = Long.parseLong(req.getParameter("id"));
        Produto produto = (Produto) dao.retrieve(id);
        req.setAttribute("produto", produto);

        List<Fornecedor> fornecedores = fornDao.retrieveAll();
        req.setAttribute("fornecedores", fornecedores);

        List<Marca> marcas = marcaDAO.retrieveAll();
        req.setAttribute("marcas",marcas);

        List<Estoque> estoques = estoqueDAO.retrieveAll();
        req.setAttribute("estoques", estoques);

        ProdutoEstoque produtoEstoque= (ProdutoEstoque) estoqueProdutoDAO.retrieve(req.getParameter("id"));
        req.setAttribute("produtoEstoque", produtoEstoque);

        ServletUtil.dispatch(req,resp, Views.UPDATE_PRODUTO);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long idProduto = Long.parseLong(req.getParameter("id"));
        Produto p = (Produto) dao.retrieve(idProduto);

        Long fornecedor = Long.parseLong(req.getParameter("fornecedor"));
        Fornecedor f = (Fornecedor) fornDao.retrieve(fornecedor);
        p.setFornecedor(f);

        Long estoque = Long.parseLong(req.getParameter("estoque"));
        Estoque e = (Estoque) estoqueDAO.retrieve(estoque);

        ProdutoEstoque produtoEstoque = (ProdutoEstoque) estoqueProdutoDAO.retrieve(idProduto);
        produtoEstoque.setQuantidade(Integer.parseInt(req.getParameter("quantidade")));
        produtoEstoque.setEstoque(e);
        estoqueProdutoDAO.update(produtoEstoque);

        p.setEstoque(e);
        p.setNome(req.getParameter("nome"));
        p.setCor(req.getParameter("cor"));
        p.setPrecoVenda(Double.parseDouble(req.getParameter("precoVenda")));
        p.setPrecoCompra(Double.parseDouble(req.getParameter("precoCompra")));
        p.setTipo(req.getParameter("tipo"));

        dao.update(p);
        resp.sendRedirect("/produto");
    }
}
